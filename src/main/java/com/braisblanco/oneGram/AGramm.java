package com.braisblanco.oneGram;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class AGramm {
	
	public static void main(String args[]) throws IOException, ClassNotFoundException, InterruptedException {
		
		Configuration conf = new Configuration();
		
		conf.set("fs.s3a.access.key", "");
		conf.set("fs.s3a.secret.key","");
		conf.set("mapreduce.map.output.compress", "true");
        conf.set("mapreduce.map.output.compression.codec", "com.hadoop.compression.lzo.LzoCodec");
		 conf.set( "dfs.blocksize", String.valueOf(128*1024*1024)) ;

		Job job = Job.getInstance(conf, "a-gramm");
		
		job.setJarByClass(AGramm.class);
		job.setMapperClass(OneGramMapper.class);
		job.setCombinerClass(OneGramCombiner.class);
		job.setReducerClass(OneGramReducer.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(Text.class);
        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        
		FileInputFormat.addInputPath(job, new Path("s3a://datasets.elasticmapreduce/ngrams/books/20090715/spa-all/1gram/data"));
		FileOutputFormat.setOutputPath(job, new Path("/user/brais/output_t2_p1"));
		
		System.out.println(job.waitForCompletion(true) ? "BIEN" : "ERROR");
		
		
	}
}
