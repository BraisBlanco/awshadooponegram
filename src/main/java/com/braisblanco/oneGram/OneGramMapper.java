package com.braisblanco.oneGram;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.sun.xml.bind.CycleRecoverable.Context;



public class OneGramMapper extends Mapper<LongWritable, Text, IntWritable, Text> {
	
	@Override
	protected void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		
		String line = value.toString();
		StringTokenizer s = new StringTokenizer(line,"\t"); 
		String oneGram = s.nextToken();
		int year = Integer.parseInt(s.nextToken());
		String count = s.nextToken();
		int decade = ((int) year / 10)*10;
		
		if(oneGram.startsWith("a") || oneGram.startsWith("á")) {
			if(year >= 1800) {
				context.write(new IntWritable(decade), new Text(oneGram.concat("\t").concat(count)));
			}
		}		
	}
}
